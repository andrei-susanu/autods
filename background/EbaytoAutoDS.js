class EbaytoAutoDS {
  constructor(email,token) {
    var obj = this;
    this.API_URL = "https://app.autods.com/";
    this.email = email;
    this.token = token;
    this.uploadStorage = 0;
    //var d = new Date();
    //this.dateoutput =d.getDate()+"-"+(d.getMonth() + 1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
    //~ this.getDate().then((resp) => {
        //~ obj.dateoutput = resp;
    //~ });
    //~ this.getValidateDate().then((resp) => {
        //~ obj.validdate = resp;
    //~ });
    this.getUpdatedDate();
    this.getUpdatedValidDate();
    this.getCrfToken().then((resp) =>{
        obj.csrftoken = resp;
    });
  }
  getUpdatedDate(){
    var d = new Date(),
    minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
    hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
    ampm = d.getHours() >= 12 ? 'pm' : 'am',
    months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
    days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
    this.dateoutput = days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+' '+d.getFullYear()+' '+hours+':'+minutes+ampm;  
  }
  getUpdatedValidDate(){
    var validdate = new Date();
    validdate.setDate(validdate.getDate() + 7);
    this.validdate = validdate.getDate()+'/'+validdate.getMonth()+'/'+validdate.getFullYear();  
  }
  getValidateDate() {
    return new Promise(resolve => {
        var validdate = new Date();
        validdate.setDate(validdate.getDate() + 7);
        resolve(validdate.getDate()+'/'+validdate.getMonth()+'/'+validdate.getFullYear());
    });   
  }
  getDate() {
    return new Promise(resolve => {
        var d = new Date(),
        minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
        hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
        ampm = d.getHours() >= 12 ? 'pm' : 'am',
        months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
        days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
        resolve(days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+' '+d.getFullYear()+' '+hours+':'+minutes+ampm);
    });   
  }
  validateToken(token){
    const URL = this.API_URL+'validate_token?token='+token;
    return new Promise(resolve => {
      try {
        const request = new XMLHttpRequest();
        request.onreadystatechange = ({ currentTarget }) => {
          const { readyState, status } = currentTarget;
          if (currentTarget.responseText.includes('Token is invalid')) {
            resolve({ status: 'error', message: 'Token is invalid' });
          }
          if (readyState !== 4 || status !== 200 ) {
              //resolve({ status: 'error', message: 'Token is invalid' });
              return;
          }
          else if (currentTarget.responseText.includes('Go to Register Page')) {
              resolve({ status: 'error', message: 'Please Login to Autods Software' });
          }
          else{
              const response = JSON.parse(currentTarget.responseText);
              resolve(response);
          }
        };
        request.open('GET', URL);
        request.send();
      } catch (e) {
        resolve(false);
      }
    })
  }
  updateLatestTokens(){
    var obj = this;  
    this.getCrfToken().then((resp) =>{
        obj.csrftoken = resp;
    });
    const keys = [
        'token',
        'email'
    ]; 
    getFromSyncStorage(keys).then((data) => {
        if( (data.email) && (data.token)){
            this.token = data.token; 
            this.email = data.email; 
        }
    });
  }
  getCrfToken(){
    return new Promise(resolve => {
        chrome.cookies.get({'url': this.API_URL, 'name': 'csrftoken'}, function(cookie) {
            if(cookie){
                resolve(cookie.value)
            }
        }); 
    });
  }
  checkEbaySignIn(){
    var URL_Ebay = "https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeUploadForm";
    return new Promise((resolve) => {
      const request = new XMLHttpRequest();
      request.onreadystatechange = (e) => {
        const target = e.currentTarget;
        if (target.readyState !== 4 || target.status !== 200) {
          return;
        }
        const { responseText } = target;
        const responseURL = target.responseURL;
        try {  
          if(responseURL.includes('//signin.ebay.com'))
            resolve(false);
          else
            resolve(true);
        } catch (e) {
          resolve(false);
        }
      };
      request.open('GET', URL_Ebay);
      request.send();
    });
  }
  processUploaded(currentTarget) {
    return new Promise(resolve => {
        const { responseURL, responseText } = currentTarget;

        const statuses = [{
          text: 'Upload Success',
          type: 'SUCCESS',
        }, {
          text: 'The system has already processed',
          type: 'ERROR',
          code: 'FILENAME',
        }, {
          text: 'Invalid email address specified',
          type: 'ERROR',
          code: 'EMAIL'
        }, {
          text: 'Upload for the day exceeded the limit',
          type: 'ERROR',
          code: 'LIMIT',
        }];

        let status = statuses.find((s) => responseText.includes(s.text));
        if (!status) {
            console.log('test2');
            console.log(responseURL);
            var signInText = "<a target='_blank' href='https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeUploadForm'>sign in to your eBay account here</a>";
          status = responseURL.includes(`//signin.ebay.com`)
            ? { text: 'Please '+signInText+' and then try again', type: 'ERROR', code: 'LOGIN' }
            : { text: 'Unknown error at uploading file at ebay, please try again', type: 'ERROR', code: 'UNKNOWN', responseText };
        }
        if (status.type === 'SUCCESS') {
          const searchedText = 'Your ref # is ';
          const refStartIndex = responseText.indexOf(searchedText) + searchedText.length;
          const refStopIndex = responseText.indexOf('.', refStartIndex);
          var ref = responseText.substring(refStartIndex, refStopIndex);
          resolve({ error: 0, message: 'Uploaded to eBay, Pending eBay Processing', reffId: ref });
          
        } else if (status.type === 'ERROR') {
            console.log('test4');
            console.log(status);
          resolve({ error: 1, message: status.text });
        }
    });
  }
  sendFileToEbay(file) {
    const URL_EBAY = `https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeUploadSuccess`;
    const formData = new FormData();
    const blob = new Blob([file], { type: 'text/csv' });
    var name = 'lister-'+Date.now()+'.csv';
    return new Promise(resolve => {
      try {
        const request = new XMLHttpRequest();
        request.onreadystatechange = ({ currentTarget }) => {
          const { readyState, status } = currentTarget;
          if (readyState !== 4 || status !== 200) {
            return;
          }
          this.processUploaded(currentTarget).then((resp) => {
            resolve(resp);  
          });
        };
        request.open('POST', URL_EBAY);
        const headers = [
          ['Pragma', 'no-cache'],
          ['Cache-Control', 'no-cache'],
          ['Upgrade-Insecure-Requests', '1'],
          ['Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'],
          ['Access-Control-Allow-Origin', '*'],
        ];
        const fData = [
          ['emailAddress', this.email],
          ['uploadFile', blob, name],
          ['Upload', 'Upload'],
          ['mid', ''],
          ['hmid', ''],
        ];

        headers.forEach(header => request.setRequestHeader(...header));
        fData.forEach(item => formData.append(...item));
        request.send(formData);
      } catch (e) {
        resolve(false);
      }
    });
  }
  getzipfilesfromAutods(){
    const URL = this.API_URL+'export_listings_to_file?token='+this.token+'&export_type=new';
    return new Promise(resolve => {
      try {
        const request = new XMLHttpRequest();
        //~ request.onreadystatechange = ({ currentTarget }) => {
          //~ const { readyState, status } = currentTarget;
          //~ //console.log(currentTarget.responseText);
          //~ if (readyState !== 4 || status !== 200 ) {
              //~ return;
          //~ } 
          //~ if (currentTarget.responseText.includes('error_message')) {
            //~ // parse json and show error here
            //~ const response = JSON.parse(currentTarget.responseText);
            //~ if(response.status != 'success'){
                //~ resolve({ error: 1, message: 'API Error,'+response.error_message });
            //~ }
          //~ }
          //~ else if (currentTarget.responseText.includes('Go to Register Page')) {
            //~ resolve({ error: 1, message: 'Please Login to Autods Software' });
          //~ }
          //~ else{ 
            //console.log(currentTarget.responseText);
            var zip = new JSZip();
            try {
                JSZipUtils.getBinaryContent(URL).then(function(data) {
                    console.log(data);
                    zip.loadAsync(data).then(function (content) {
                        console.log(content);
                        var files = Object.keys(zip.files);
                        console.log(files.length);
                        if(files.length == 0){
                            resolve({ error: 1, message: 'No Pending Uploads found' });
                        }
                        var csvarray=[];
                        files.forEach(function(item,lid,files) {
                            zip.file(item).async("text").then(function success(txt) {
                              csvarray.push(txt);
                              if(lid === files.length - 1){
                                 //resolve(csvarray); 
                                 resolve({ error: 0, csv: csvarray });
                              }
                            }, function error(e) {
                                console.log('error');
                                resolve({ error: 1, message: 'No Pending Uploads found' });
                            });
                        });          
                    });
                });
            } catch (e) {
                resolve({ error: 1, message: 'Api Error' });
            }
            //resolve({ error: 0, csv: currentTarget.responseText });
          //~ }
        //~ };
        //~ request.open('GET', URL);
        //~ request.setRequestHeader('X_CSRF_TOKEN', this.csrftoken);
        //~ request.send();
      } catch (e) {
        resolve(false);
      }
    })
  }
  uploadToEbayZip(){
    return new Promise(resolve => {
        this.getUpdatedDate();
        this.getUpdatedValidDate();
        this.checkEbaySignIn().then((resp) => {
            if(resp){
                this.getzipfilesfromAutods().then((resp) => {
                    console.log(resp); 
                    if(resp.error === 0) {
                        var $this = this;
                        var csvcollect = resp.csv;
                        var uploadedReffIds = [];
                        var productInCsvArray = localStorage.getItem('uploadedReffIdsTemp')
                        if(productInCsvArray){
                            if(JSON.parse(productInCsvArray).length){
                                uploadedReffIds = JSON.parse(productInCsvArray);
                            }
                        }
                        var counter = 0;
                        var obj = this;
                        csvcollect.forEach(function(csv,lid, csvcollect) {
                            $this.sendFileToEbay(csv).then((resp1) => {
                                if(resp1){
                                    if(resp1.error == 1){
                                        console.log(resp1.message);
                                        // through error in button log that this file is missed to upload
                                        setStatusLogs({ 'logs': true, 'valid_date': obj.validdate, 'Button': 'Upload pending uploads to ebay', 'time': obj.dateoutput, 'message': resp1.message},'btnlogs');
                                        resolve(resp1);
                                    }
                                    else{
                                        counter++;
                                        uploadedReffIds.push(resp1.reffId); 
                                        if(counter === csvcollect.length){
                                            console.log(uploadedReffIds);
                                            localStorage.setItem('uploadedReffIdsTemp', JSON.stringify(uploadedReffIds));
                                            localStorage.setItem('uploadedReffIds', JSON.stringify(uploadedReffIds));
                                            resolve(resp1);
                                        }     
                                    }
                                }
                                else{
                                    setStatusLogs({ 'logs': true, 'valid_date': obj.validdate, 'Button': 'Upload pending uploads to ebay', 'time': obj.dateoutput, 'message': 'Unknown Error, Please contact to Autods Team'},'btnlogs');
                                    resolve({ error: 1, message: "Unknown Error, Please contact to Autods Team" }); 
                                }
                            });
                        }); 
                          
                    }else {
                      setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Upload pending uploads to ebay', 'time': this.dateoutput, 'message': resp.message},'btnlogs');
                      resolve({ error: 1, message: resp.message });
                    }
                });
            }else{
                var signInText = "<a target='_blank' href='https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeUploadForm'>sign in to your eBay account here</a>";
                setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Upload pending uploads to ebay', 'time': this.dateoutput, 'message': 'Please '+signInText+' and then try again'},'btnlogs');
                resolve({ error: 1, message: 'Please '+signInText+' and then try again' });
            }
        });
    });  
  }

  monitorOrderSyncNow(){
    return new Promise(resolve => {
        if (this.intervalMonitorOrderSyncNow) {
            clearInterval(this.intervalMonitorOrderSyncNow);
        }
        this.intervalMonitorOrderSyncNow = setInterval(() => {
            console.log('monitor Order Sync Now working');
            this.getUpdatedDate();
            this.getUpdatedValidDate();
            // get sold download request reff id (soldDownloadRequestReffId) from local storage
            getFromSyncStorage('soldDownloadRequestReffId').then((localReffId) => {
                this.downloadLastSoldReadyReport(localReffId).then((resp) => {
                    var reffId = resp.reffId;
                    if(reffId){
                        if(resp.message == 'do nothing'){
                            console.log('The File with Reff id: '+reffId+' is not completed its processing yet, Scheduler will try again in next cycle');
                            resolve({ error: 1, message: 'The File with Reff id: '+reffId+' is not completed its processing yet, Monitor will try again in next cycle' });
                        }
                        else{
                            this.getFileFromEbay(reffId).then((csv) => {
                                this.sendFileToAutods(csv,reffId,'upload_orders_file').then((resp) => {
                                    console.log(resp);
                                    if(resp){
                                        if(resp.status == 'success'){
                                            // remove sold download request reff id from local storage and clear the interval
                                            setToSyncStorage({ soldDownloadRequestReffId: false });
                                            if (this.intervalMonitorOrderSyncNow) {
                                                clearInterval(this.intervalMonitorOrderSyncNow);
                                            }
                                            resolve({ error: 0, message: resp.error_message });
                                        }
                                        else{
                                            if(resp.status == 1){
                                                setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'monitor Order Sync Now', 'time': this.dateoutput, 'message': resp.message},'btnlogs');
                                                resolve({ error: 1, message: resp.message });
                                            }
                                            else{
                                                setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'monitor Order Sync Now', 'time': this.dateoutput, 'message': resp.error_message},'btnlogs');
                                                resolve({ error: 1, message: resp.error_message });
                                            }
                                        }
                                    }
                                });
                            });
                        }
                    }
                    else{
                        var signInText = "<a target='_blank' href='https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadRequest'>sign in to your eBay account here</a>";
                        setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'monitor Order Sync Now', 'time': this.dateoutput, 'message': 'Please '+signInText+' and then try again'},'btnlogs');
                    }
                });
            });
        },60000); // will start when "Run order Sync Now" button clicked ,run every 1 minute, i.e 600000 milliseconds, and stop when file completed and successfully updated back to autods
    });  
  }
  monitorUntrackedSyncNow(){
    return new Promise(resolve => {
        if (this.intervalMonitorUntrackedSyncNow) {
            clearInterval(this.intervalMonitorUntrackedSyncNow);
        }
        this.intervalMonitorUntrackedSyncNow = setInterval(() => {
            console.log('monitor Untracked Sync Now working');
            this.getUpdatedDate();
            this.getUpdatedValidDate();
            // get active download request reff id (activeDownloadRequestReffId) from local storage
            getFromSyncStorage('activeDownloadRequestReffId').then((localReffId) => {
                this.downloadLastActiveReadyReport(localReffId).then((resp) => {
                    var reffId = resp.reffId;
                    if(reffId){
                        if(resp.message == 'do nothing'){
                            console.log('The File with Reff id: '+reffId+' is not completed its processing yet, Scheduler will try again in next cycle');
                            resolve({ error: 1, message: 'The File with Reff id: '+reffId+' is not completed its processing yet, Monitor will try again in next cycle' });
                        }
                        else{
                            this.getFileFromEbay(reffId).then((csv) => {
                                this.sendFileToAutods(csv,reffId,'upload_active_items_file').then((resp) => {
                                    console.log(resp);
                                    if(resp){
                                        if(resp.status == 'success'){
                                            // remove active download request reff id from local storage and clear the interval
                                            setToSyncStorage({ activeDownloadRequestReffId: false });
                                            if (this.intervalMonitorUntrackedSyncNow) {
                                                clearInterval(this.intervalMonitorUntrackedSyncNow);
                                            }
                                            resolve({ error: 0, message: resp.error_message });
                                        }
                                        else{
                                            if(resp.status == 1){
                                                setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'monitor Untracked Sync Now', 'time': this.dateoutput, 'message': resp.message},'btnlogs');
                                                resolve({ error: 1, message: resp.message });
                                            }
                                            else{
                                                setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'monitor Untracked Sync Now', 'time': this.dateoutput, 'message': resp.error_message},'btnlogs');
                                                resolve({ error: 1, message: resp.error_message });
                                            }
                                        }
                                    }
                                });
                            });
                        }
                    }
                    else{
                        var signInText = "<a target='_blank' href='https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadRequest'>sign in to your eBay account here</a>";
                        setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'monitor Untracked Sync Now', 'time': this.dateoutput, 'message': 'Please '+signInText+' and then try again'},'btnlogs');
                    }
                });
            });
        },60000); // will start when "Run Untracked Sync Now" button clicked ,run every 1 minute, i.e 600000 milliseconds, and stop when file completed and successfully updated back to autods
    });  
  }
  monitorUploadStatuses(){
    return new Promise(resolve => {
        if (this.intervalMonitorUploadStatuses) {
            clearInterval(this.intervalMonitorUploadStatuses);
        }
        this.intervalMonitorUploadStatuses = setInterval(() => {
            var d = new Date(),
            minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
            hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
            ampm = d.getHours() >= 12 ? 'pm' : 'am',
            months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
            this.dateoutput = days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+' '+d.getFullYear()+' '+hours+':'+minutes+ampm;
            var validdate = new Date();
            validdate.setDate(validdate.getDate() + 7);
            this.validdate = validdate.getDate()+'/'+validdate.getMonth()+'/'+validdate.getFullYear();
            var uploadedReffIdsTemp = localStorage.getItem('uploadedReffIdsTemp');
            if(uploadedReffIdsTemp){
                uploadedReffIdsTemp = JSON.parse(uploadedReffIdsTemp);
                console.log(uploadedReffIdsTemp);
                var counter = 0;
                if(uploadedReffIdsTemp.length > 0){
                    var obj = this;
                    $(uploadedReffIdsTemp).each(function(index,value){
                        obj.getUploadStatussesfiles(value).then((resp) => {
                            counter++;
                            if(counter == uploadedReffIdsTemp.length){
                                console.log('all request executed successfully for upload monitor');
                            }
                        }); 
                    });   
                }
                else{
                    console.log('monitor upload status: no new uploads found to update on autods');
                }
            }
            else{
                console.log('monitor upload status: no new uploads found to update on autods');
            }
        },300000); // run every 5 minutes, i.e 300000 milliseconds, it will never stop
    });
  }
  getFileFromAutods() {
    const URL = this.API_URL+'export_active_listings?token='+this.token;
    return new Promise(resolve => {
      try {
        const request = new XMLHttpRequest();
        request.onreadystatechange = ({ currentTarget }) => {
          const { readyState, status } = currentTarget;
          if (readyState !== 4 || status !== 200 ) {
              return;
          }
          if (currentTarget.responseText.includes('error_message')) {
            // parse json and show error here
            const response = JSON.parse(currentTarget.responseText);
            if(response.status != 'success'){
                resolve({ error: 1, message: 'API Error,'+response.error_message });
            }
            else{
                resolve({ error: 0, csv: currentTarget.responseText });
            }
          }
          else if (currentTarget.responseText.includes('Go to Register Page')) {
            resolve({ error: 1, message: 'Please Login to Autods Software' });
          }
          else{
            resolve({ error: 0, csv: currentTarget.responseText });
          }
        };
        request.open('GET', URL);
        request.setRequestHeader('X_CSRF_TOKEN', this.csrftoken);
        request.send();
      } catch (e) {
        resolve(false);
      }
    })
  }
  uploadToEbay(){
    return new Promise(resolve => {
        this.checkEbaySignIn().then((resp) => {
            if(resp){
                this.getFileFromAutods().then((resp) => {
                    console.log(resp);  
                    if(resp.error == 0) {
                        console.log(resp.csv);
                        this.sendFileToEbay(resp.csv)
                        .then((resp1) => {
                            if(resp1){
                                setToSyncStorage({ lastUploadTime: Date.now() });
                                resolve(resp1);
                            }
                            else{
                                resolve({ error: 1, message: "Unknown Error, Please contact to Autods Team" }); 
                            }
                        });
                    } else {
                      resolve({ error: 1, message: resp.message });
                    }
                });
            }
            else{
                var signInText = "<a target='_blank' href='https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeUploadForm'>sign in to your eBay account here</a>";
                resolve({ error: 1, message: 'Please '+signInText+' and then try again' });
            }
        });
    });  
  }
  startuploadToEbayScheduleSync(param = ''){
    var schedularIntervalTime; 
    if(param == 'runNow')
        schedularIntervalTime = 3600000;
    else
        schedularIntervalTime = param;
    return new Promise(resolve => {
        
        this.getUpdatedDate();
        this.getUpdatedValidDate();
        setToSyncStorage({ startuploadToEbayScheduleSync: true });
        
        if (this.intervalUploadFile) {
            clearInterval(this.intervalUploadFile);
        }
        getFromSyncStorage(['startActiveScheduleSync','startSoldScheduleSync','startuploadToEbayScheduleSync']).then((data) => {
            if( (data.startActiveScheduleSync) && (data.startSoldScheduleSync) && (data.startuploadToEbayScheduleSync) ){
                chrome.browserAction.setBadgeText ( { text: "" } );
            }
        });
        if(param == 'runNow'){
            this.uploadToEbay().then((resp) => {
                if(resp.error == 1){
                    resolve({ error: 1, message: resp.message });
                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Upload', 'time': this.dateoutput, 'message': resp.message},'logs');
                }
            });
        }
        this.intervalUploadFile = setInterval(() => {
            console.log('upload scheduler');
            schedularIntervalTime = 3600000;
            this.getUpdatedDate();
            this.getUpdatedValidDate();
            this.uploadToEbay().then((resp) => {
                if(resp.error == 1){
                    resolve({ error: 1, message: resp.message });
                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Upload', 'time': this.dateoutput, 'message': resp.message},'logs');
                }
                else if(resp.error == 0){
                    // save reff id to storage
                    //setToSyncStorage({ lastUploadReportReffID: reffId });
                    setToSyncStorage({ lastUploadTime: Date.now() });
                    resolve({ error: 0, message: resp.message });
                }
            });
            if (this.intervalUploadFile) {
                clearInterval(this.intervalUploadFile);
            }
            this.startuploadToEbayScheduleSync(3600000);
        },schedularIntervalTime);  // every 60 minutes (3600000) //60000
    });  
  }
  stopuploadToEbayScheduleSync() {
    return new Promise(resolve => {
        if (this.intervalUploadFile) {
            clearInterval(this.intervalUploadFile);
            this.intervalUploadFile = 0;
            setToSyncStorage({ startuploadToEbayScheduleSync: false });
            resolve({ error: 0, message: 'stop' });
        }
        else{
            clearInterval(this.intervalUploadFile);
            this.intervalUploadFile = 0;
            setToSyncStorage({ startuploadToEbayScheduleSync: false });
            resolve({ error: 0, message: 'already_stopped' });
        } 
        chrome.browserAction.setBadgeBackgroundColor({ color: '#dcaa70' });
        chrome.browserAction.setBadgeText ( { text: "Stop" } );
    });
  }
  sendUploadResponseToAutods(customLabel,status,itemId,message) {
    const token = this.token;
    const URL = this.API_URL+'update_scheduled_item_status/';
   
    const formData = new FormData();
    return new Promise( (resolve,reject) => {
      const request = new XMLHttpRequest();
      request.onreadystatechange = (e) => {
        const target = e.currentTarget;
        if(target.status == 500){
            setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Upload Pending Uploads To eBay Monitoring', 'time': this.dateoutput, 'message': 'API (update_scheduled_item_status) returning 500 error'},'btnlogs');
            resolve({ status: 1, message: 'API (update_scheduled_item_status) returning 500 error'});
            
        }
        if (target.readyState !== 4 || target.status !== 200) {
          return;
        }
        if (target.responseText.includes('error_message')) {
            const response = JSON.parse(target.responseText);
            if(response.status != 'success'){
                resolve({ status: 1, message: 'API Error,'+response.error_message });
            }
            else{
                resolve(response);
            }
        }
        else if (target.responseText.includes('Go to Register Page')) {
            resolve({ status: 1, message: 'Please Login to Autods Software' });
        }
        else{
            const response = JSON.parse(target.responseText);
            resolve(response);
        }
      };
      request.open('POST', URL);
      request.setRequestHeader('X_CSRF_TOKEN', this.csrftoken);
      formData.append('csrfmiddlewaretoken', this.csrftoken);
      formData.append('token', token); 
      formData.append('item_id', customLabel); 
      formData.append('success', status); 
      formData.append('message', message); 
      if(itemId)
        formData.append('uploaded_item_id', itemId ); 
      request.send(formData);
    });
  }
  sendFileToAutods(csv,reffId,api) {
    const token = this.token;
    const URL = this.API_URL+api+'/';
    const blob = new Blob([csv], { type: 'text/csv' });
    const formData = new FormData();
    return new Promise( (resolve,reject) => {
      const request = new XMLHttpRequest();

      request.onreadystatechange = (e) => {
        const target = e.currentTarget;
        if (target.readyState !== 4 || target.status !== 200) {
          return;
        }
        if (target.responseText.includes('error_message')) {
            const response = JSON.parse(target.responseText);
            if(response.status != 'success'){
                resolve({ status: 1, message: 'API Error,'+response.error_message });
            }
            else{
                resolve(response);
            }
        }
        else if (target.responseText.includes('Go to Register Page')) {
            resolve({ status: 1, message: 'Please Login to Autods Software' });
        }
        else{
            const response = JSON.parse(target.responseText);
            resolve(response);
        }
      };
      request.open('POST', URL);
      request.setRequestHeader('X_CSRF_TOKEN', this.csrftoken);
      formData.append('csrfmiddlewaretoken', this.csrftoken);
      formData.append('user_token', token); 
      formData.append('uploadfile', blob, "download_"+reffId+".csv"); 
      request.send(formData);
    });
  }
  runSoldDownloadNow(){
    return new Promise(resolve => {
        this.downloadLastSoldReadyReport().then((resp) => {
            console.log(resp);
            if(resp.message == 'signIn'){
                var signInText = "<a target='_blank' href='https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadRequest'>sign in to your eBay account here</a>";
                resolve({ error: 1, message: 'Please '+signInText+' and then try again' });
            }
            var reffId = resp.reffId;
            if(reffId){
                getFromSyncStorage('lastSoldReportReffID')
                .then((lastSoldReportReffID) => {
                    if (lastSoldReportReffID == reffId) {
                        console.log(lastSoldReportReffID);
                        this.downloadSoldReport(true);
                        resolve({ error: 1, message: 'The File with Reff id: '+reffId+' already_downloaded and uploaded to server' });
                    } else {
                        if(resp.message == 'do nothing'){
                            resolve({ error: 1, message: 'The File with Reff id: '+reffId+' is not completed its processing yet, Scheduler will try again in next cycle' });
                        }
                        else{
                            this.getFileFromEbay(reffId).then((csv) => {
                                console.log(csv);
                                var encodedUri = encodeURI(csv);
                                //var encodedUri = 'https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeDownload&RefId='+reffId;
                                var link = document.createElement("a");
                                link.setAttribute("href", encodedUri);
                                link.setAttribute("download", reffId+".csv");
                                document.body.appendChild(link); 
                                //link.click(); // This will download the data file named "my_data.csv".
                                this.sendFileToAutods(csv,reffId,'upload_orders_file').then((resp) => {
                                    console.log(resp);
                                    if(resp){
                                        if(resp.status == 'success'){
                                            this.downloadSoldReport(true);
                                            // save reff id to storage
                                            setToSyncStorage({ lastSoldReportReffID: reffId });
                                            setToSyncStorage({ lastSoldDownloadTime: Date.now() });
                                            resolve({ error: 0, message: resp.error_message });
                                        }
                                        else{
                                            if(resp.status == 1){
                                                resolve({ error: 1, message: resp.message });
                                            }
                                            else{
                                                resolve({ error: 1, message: resp.error_message });
                                            }
                                        }
                                    }
                                    
                                }); 
                            });
                        }
                    }
                });

            }
        }); 
    });
      
  }
  startSoldScheduleSync(param = ''){
    var schedularIntervalTime; 
    if(param == 'runNow')
        schedularIntervalTime = 1200000;
    else
        schedularIntervalTime = param;
    
    return new Promise(resolve => {
        this.getUpdatedDate();
        this.getUpdatedValidDate();
        setToSyncStorage({ startSoldScheduleSync: true });
        
        if (this.intervalCheckFileSold) {
            clearInterval(this.intervalCheckFileSold);
        }
        getFromSyncStorage(['startActiveScheduleSync','startSoldScheduleSync','startuploadToEbayScheduleSync']).then((data) => {
            if( (data.startActiveScheduleSync) && (data.startSoldScheduleSync) && (data.startuploadToEbayScheduleSync) ){
                chrome.browserAction.setBadgeText ( { text: "" } );
            }
        });
        if(param == 'runNow'){
            
            this.runSoldDownloadNow().then((resp) => {
                if(resp.error == 1){
                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Sold', 'time': this.dateoutput, 'message': resp.message},'logs');
                    resolve({ error: 1, message: resp.message });
                }
            });
        }
        this.intervalCheckFileSold = setInterval(() => {
            console.log('sold scheduler');
            this.getUpdatedDate();
            this.getUpdatedValidDate();
            this.downloadLastSoldReadyReport().then((resp) => {
                var reffId = resp.reffId;
                if(reffId){
                    setToSyncStorage({ startSoldScheduleSync: true });
                    getFromSyncStorage('lastSoldReportReffID')
                    .then((lastSoldReportReffID) => {
                        if (lastSoldReportReffID == reffId) {
                            this.downloadSoldReport(true);
                            setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Sold', 'time': this.dateoutput, 'message': 'The File with Reff id: '+reffId+' already_downloaded and uploaded to server'},'logs');
                            resolve({ error: 1, message: 'The File with Reff id: '+reffId+' already_downloaded and uploaded to server' });
                        } else {
                            if(resp.message == 'do nothing'){
                                setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Sold', 'time': this.dateoutput, 'message': 'The File with Reff id: '+reffId+' is not completed its processing yet, Scheduler will try again in next cycle'},'logs');
                                resolve({ error: 1, message: 'The File with Reff id: '+reffId+' is not completed its processing yet, Scheduler will try again in next cycle' });
                            }
                            else{
                                this.getFileFromEbay(reffId).then((csv) => {
                                    var encodedUri = encodeURI(csv);
                                    //var encodedUri = 'https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeDownload&RefId='+reffId;
                                    var link = document.createElement("a");
                                    link.setAttribute("href", encodedUri);
                                    link.setAttribute("download", reffId+".csv");
                                    document.body.appendChild(link); 
                                    //link.click(); // This will download the data file named "my_data.csv".
                                    this.sendFileToAutods(csv,reffId,'upload_orders_file').then((resp) => {
                                        if(resp){
                                            if(resp.status == 'success'){
                                                this.downloadSoldReport(true);
                                                // save reff id to storage
                                                setToSyncStorage({ lastSoldReportReffID: reffId });
                                                setToSyncStorage({ lastSoldDownloadTime: Date.now() });
                                                resolve({ error: 0, message: resp.error_message });
                                            }
                                            else{
                                                if(resp.status == 1){
                                                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Sold', 'time': this.dateoutput, 'message': resp.message},'logs');
                                                    resolve({ error: 1, message: resp.message });
                                                }
                                                else{
                                                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Sold', 'time': this.dateoutput, 'message': resp.error_message},'logs');
                                                    resolve({ error: 1, message: resp.error_message });
                                                }
                                            }
                                        }
                                        else{
                                           setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Sold', 'time': this.dateoutput, 'message': "Unknown Error: Please contact to autods team, Schedular will run will try in next cycle again."},'logs'); 
                                        }
                                    }); 
                                    
                                });
                            }
                        }
                    });
                }
                else{
                    var signInText = "<a target='_blank' href='https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadRequest'>sign in to your eBay account here</a>";
                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Sold', 'time': this.dateoutput, 'message': 'Please '+signInText+' and then try again'},'logs');
                }
                if (this.intervalCheckFileSold) {
                    clearInterval(this.intervalCheckFileSold);
                }
                this.startSoldScheduleSync(1200000);
                
            });
        },schedularIntervalTime);  // every 20 minutes (1200000) /20000
    });
    
  }
  stopSoldScheduleSync() {
    return new Promise(resolve => {
        if (this.intervalCheckFileSold) {
            clearInterval(this.intervalCheckFileSold);
            this.intervalCheckFileSold = 0;
            setToSyncStorage({ startSoldScheduleSync: false });
            resolve({ error: 0, message: 'stop' });
        }
        else{
            clearInterval(this.intervalCheckFileSold);
            this.intervalCheckFileSold = 0;
            setToSyncStorage({ startSoldScheduleSync: false });
            resolve({ error: 0, message: 'already_stopped' });
        }  
        chrome.browserAction.setBadgeBackgroundColor({ color: '#dcaa70' });
        chrome.browserAction.setBadgeText ( { text: "Stop" } );
    });
  }
  runActiveDownloadNow(){
    return new Promise(resolve => {
        this.downloadLastActiveReadyReport().then((resp) => {
            console.log(resp);
            if(resp.message == 'signIn'){
                var signInText = "<a target='_blank' href='https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadRequest'>sign in to your eBay account here</a>";
                resolve({ error: 1, message: 'Please '+signInText+' and then try again' });
            }
            var reffId = resp.reffId;
            if(reffId){
                getFromSyncStorage('lastActiveReportReffID')
                .then((lastActiveReportReffID) => {
                    if (lastActiveReportReffID == reffId) {
                        this.downloadActiveReport(true);
                        resolve({ error: 1, message: 'The File with Reff id: '+reffId+' already_downloaded and uploaded to server' });
                    } else {
                        if(resp.message == 'do nothing'){
                            resolve({ error: 1, message: 'The File with Reff id: '+reffId+' is not completed its processing yet, Scheduler will try again in next cycle' });
                        }
                        else{
                            this.getFileFromEbay(reffId).then((csv) => {
                                console.log(csv);
                                var encodedUri = encodeURI(csv);
                                //var encodedUri = 'https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeDownload&RefId='+reffId;
                                var link = document.createElement("a");
                                link.setAttribute("href", encodedUri);
                                link.setAttribute("download", reffId+".csv");
                                document.body.appendChild(link); // Required for FF
                                //link.click(); // This will download the data file named "my_data.csv".
                                this.sendFileToAutods(csv,reffId,'upload_active_items_file').then((resp) => {
                                    console.log(resp);
                                    if(resp){
                                        if(resp.status == 'success'){
                                            this.downloadActiveReport(true);
                                            // save reff id to storage
                                            setToSyncStorage({ lastActiveReportReffID: reffId });
                                            setToSyncStorage({ lastActiveDownloadTime: Date.now() });
                                            resolve({ error: 0, message: resp.error_message });
                                        }
                                        else{
                                            if(resp.status == 1){
                                                resolve({ error: 1, message: resp.message });
                                            }
                                            else{
                                                resolve({ error: 1, message: resp.error_message });
                                            }
                                        }
                                    }
                                    else{
                                       setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Active', 'time': this.dateoutput, 'message': "Unknown Error: Please contact to autods team, Schedular will run will try in next cycle again."},'logs'); 
                                    }
                                }); 
                                
                            });
                        }
                    }   
                }) 
            }
        }); 
    });
  }
  startActiveScheduleSync(param = ''){
    var schedularIntervalTime; 
    if(param == 'runNow')
        schedularIntervalTime = 3600000;
    else
        schedularIntervalTime = param; // default time
    return new Promise(resolve => {
        this.getUpdatedDate();
        this.getUpdatedValidDate();
        setToSyncStorage({ startActiveScheduleSync: true });  
        if (this.intervalCheckFileActive) {
            clearInterval(this.intervalCheckFileActive);
        }
        getFromSyncStorage(['startActiveScheduleSync','startSoldScheduleSync','startuploadToEbayScheduleSync']).then((data) => {
            if( (data.startActiveScheduleSync) && (data.startSoldScheduleSync) && (data.startuploadToEbayScheduleSync) ){
                chrome.browserAction.setBadgeText ( { text: "" } );
            }
        });
        if(param == 'runNow'){
            
            this.runActiveDownloadNow().then((resp) => {
                if(resp.error == 1){
                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Active', 'time': this.dateoutput, 'message': resp.message},'logs');
                    resolve({ error: 1, message: resp.message }); 
                    //setToSyncStorage({ startSoldScheduleSync: true });
                    //this.stopSoldScheduleSync();
                }
            });
        }
        this.intervalCheckFileActive = setInterval(() => {
            console.log('active scheduler');
            this.getUpdatedDate();
            this.getUpdatedValidDate();
            this.downloadLastActiveReadyReport().then((resp) => {
                console.log(resp);
                var reffId = resp.reffId;
                if(reffId){
                    setToSyncStorage({ startActiveScheduleSync: true });
                    getFromSyncStorage('lastActiveReportReffID')
                    .then((lastActiveReportReffID) => {
                        if (lastActiveReportReffID == reffId) {
                            this.downloadActiveReport(true);
                            resolve({ error: 1, message: 'The File with Reff id: '+reffId+' already_downloaded and uploaded to server' });
                        } else {
                            if(resp.message == 'do nothing'){
                                resolve({ error: 1, message: 'The File with Reff id: '+reffId+' is not completed its processing yet, Scheduler will try again in next cycle' });
                            }
                            else{
                                this.getFileFromEbay(reffId).then((csv) => {
                                    console.log(csv);
                                    var encodedUri = encodeURI(csv);
                                    //var encodedUri = 'https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeDownload&RefId='+reffId;
                                    var link = document.createElement("a");
                                    link.setAttribute("href", encodedUri);
                                    link.setAttribute("download", reffId+".csv");
                                    document.body.appendChild(link); // Required for FF
                                    //link.click(); 
                                    this.sendFileToAutods(csv,reffId,'upload_active_items_file').then((resp) => {
                                        console.log(resp);
                                        if(resp){
                                            if(resp.status == 'success'){
                                                this.downloadActiveReport(true);
                                                // save reff id to storage
                                                setToSyncStorage({ lastActiveReportReffID: reffId });
                                                setToSyncStorage({ lastActiveDownloadTime: Date.now() });
                                                resolve({ error: 0, message: resp.error_message });
                                            }
                                            else{
                                                if(resp.status == 1){
                                                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Active', 'time': this.dateoutput, 'message': resp.message},'logs');
                                                    resolve({ error: 1, message: resp.message });
                                                }
                                                else{
                                                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Active', 'time': this.dateoutput, 'message': resp.error_message},'logs');
                                                    resolve({ error: 1, message: resp.error_message });
                                                }
                                            }
                                        }
                                    });
                                });
                            }
                        }   
                    }) 

                }
                else{
                    var signInText = "<a target='_blank' href='https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadRequest'>sign in to your eBay account here</a>";
                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Active', 'time': this.dateoutput, 'message': 'Please '+signInText+' and then try again'},'logs');
                }
            });
            if (this.intervalCheckFileActive) {
                clearInterval(this.intervalCheckFileActive);
            }
            this.startActiveScheduleSync(3600000);
        },schedularIntervalTime);  // every 60 minutes (3600000) //60000
    });
  }
  stopActiveScheduleSync() {
    return new Promise(resolve => {
        if (this.intervalCheckFileActive) {
            clearInterval(this.intervalCheckFileActive);
            this.intervalCheckFileActive = 0;
            setToSyncStorage({ startActiveScheduleSync: false });
            resolve({ error: 0, message: 'stop' });
        }
        else{
            clearInterval(this.intervalCheckFileActive);
            this.intervalCheckFileActive = 0;
            setToSyncStorage({ startActiveScheduleSync: false });
            resolve({ error: 0, message: 'already_stopped' });
        } 
        chrome.browserAction.setBadgeBackgroundColor({ color: '#dcaa70' });
        chrome.browserAction.setBadgeText ( { text: "Stop" } );
    });
  }
  getFileFromEbayforUploads(URL) {
    const params = new URLSearchParams();
    console.log(URL);
    return new Promise((resolve) => {
      const request = new XMLHttpRequest();
      request.onreadystatechange = (e) => {
        const target = e.currentTarget;
        if (target.readyState !== 4 || target.status !== 200) {
          //setStatusLogs({ 'logs': true, 'Schedular': 'Sold', 'time': this.dateoutput, 'message': 'Ebay API Error'});
          return;
        }
        if (target.responseText.includes('The file server is currently unavailable. Please try again after some time.')) {
          setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Upload pending uploads to ebay onitoring', 'time': this.dateoutput, 'message': 'The Ebay file server is currently unavailable. Scheduar will try again in next cycle'},'btnlogs');
          return;
        }
        resolve(target.responseText);
      };
      request.open('GET', URL);
      request.setRequestHeader('Pragma', 'no-cache');
      request.setRequestHeader('Cache-Control', 'no-cache');
      request.setRequestHeader('Upgrade-Insecure-Requests', '1');
      request.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
      request.setRequestHeader('Access-Control-Allow-Origin', '*');
      request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      request.send(params.toString());
    });  
  }
  getFileFromEbay(ref) {
    const URL = `https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeDownload&RefId=${ref}`;
    const params = new URLSearchParams();
    console.log(URL);
    return new Promise((resolve) => {
      const request = new XMLHttpRequest();

      request.onreadystatechange = (e) => {
        const target = e.currentTarget;
        if (target.readyState !== 4 || target.status !== 200) {
          //setStatusLogs({ 'logs': true, 'Schedular': 'Sold', 'time': this.dateoutput, 'message': 'Ebay API Error'});
          return;
        }

        if (target.responseText.includes('The file server is currently unavailable. Please try again after some time.')) {
          setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Schedular': 'Sold', 'time': this.dateoutput, 'message': 'The Ebay file server is currently unavailable. Scheduar will try again in next cycle'},'logs');
          return;
        }

        resolve(target.responseText);
      };

      request.open('GET', URL);

      request.setRequestHeader('Pragma', 'no-cache');
      request.setRequestHeader('Cache-Control', 'no-cache');
      request.setRequestHeader('Upgrade-Insecure-Requests', '1');
      request.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
      request.setRequestHeader('Access-Control-Allow-Origin', '*');
      request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      request.send(params.toString());
    });
  }

  callbackDownloadRequest(target) {
    const responseText = target.responseText;
    const responseURL = target.responseURL;

    if (responseText.includes('Your ref # is ')) {
      const searchedText = 'Your ref # is ';
      const refStartIndex = responseText.indexOf(searchedText) + searchedText.length;
      const refStopIndex = responseText.indexOf('.', refStartIndex);
      const ref = responseText.substring(refStartIndex, refStopIndex);
      return({ error: 0, message: 'Download link sent to the email successfully.', reffId: ref });
    } else if (responseText.includes('We are currently processing')) {
      return { error: 1, message: 'Download in process' };
    } else if (responseText.includes('Please enter a valid email address')) {
      return ({ error: 1, message: 'Invalid_email' });
    } else if (responseURL.includes('//signin.ebay.com')) {
      var signInText = "<a target='_blank' href='https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadRequest'>sign in to your eBay account here</a>";
      return ({ error: 1, message: 'Please '+signInText+' and then try again', url: responseURL });
    } else {
      return ({ error: 1, message: 'unknown error' });
    }
  }
    getEbayCodeSRT() {
        const URL_Ebay = 'https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadRequest&ssPageName=STRK:ME:LNLK';

        return new Promise((resolve) => {
          const request = new XMLHttpRequest();

          request.onreadystatechange = (e) => {
            const target = e.currentTarget;
            if (target.readyState !== 4 || target.status !== 200) {
              return;
            }

            const { responseText } = target;
            try {
              const srt = responseText.match(/name="srt"[^>]*value="([0-9a-zA-Z][^"]*)".*>/)[1];
              resolve(srt);
            } catch (e) {
              resolve(null);
            }
          };

          request.open('GET', URL_Ebay);
          request.send();
        });
    }
    downloadActiveReport(schedular = '') {
        $('#message').find('span').text('');
        const URL_Ebay = 'https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadRequestSuccess';
        const params = new URLSearchParams();
        this.getUpdatedDate();
        this.getUpdatedValidDate();
        return new Promise((resolve) => {
          this.getEbayCodeSRT()
            .then((srt) => {
                const request = new XMLHttpRequest();

                request.onreadystatechange = (e) => {
                  const target = e.currentTarget;
                  if (target.readyState !== 4 || target.status !== 200) {
                    return;
                  }

                  const ret = this.callbackDownloadRequest(target);
                  if(ret.error  == 1){
                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Run Untracked Sync Now', 'time': this.dateoutput, 'message': ret.message},'btnlogs');
                  }
                  if(!schedular && ret.error == 0){
                      console.log(ret.reffId);
                      //store active download request reff id to local storage
                      setToSyncStorage({ activeDownloadRequestReffId: ret.reffId });
                      this.monitorUntrackedSyncNow();
                  }
                  resolve(ret);
                };

                request.open('POST', URL_Ebay);

                request.setRequestHeader('Pragma', 'no-cache');
                request.setRequestHeader('Cache-Control', 'no-cache');
                request.setRequestHeader('Upgrade-Insecure-Requests', '1');
                request.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
                request.setRequestHeader('Access-Control-Allow-Origin', '*');
                request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                params.append('EmailAddress', this.email);
                params.append('isChangedView', '1');
                params.append('prevHiddenListingFilter', '3');
                params.append('ListingFilter', '3');
                params.append('DownloadFormatType', '2');
                params.append('DateRangeType', '1');
                params.append('FEActiveDownloadType', '1');
                params.append('FESoldDownloadType', '1');
                params.append('srt', srt);

                request.send(params.toString());
                
            });
        });
    }
    downloadSoldReport(schedular = '') {
        $('#message').find('span').text('');
        const URL_Ebay = 'https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadRequestSuccess';
        const params = new URLSearchParams();
        this.getUpdatedDate();
        this.getUpdatedValidDate();
        return new Promise((resolve) => {
          this.getEbayCodeSRT()
            .then((srt) => {   
                const request = new XMLHttpRequest();
                request.onreadystatechange = (e) => {
                  const target = e.currentTarget;
                  if (target.readyState !== 4 || target.status !== 200) {
                    return;
                  }

                  const ret = this.callbackDownloadRequest(target);
                  if(ret.error  == 1){
                    setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Run Orders Sync Now (Last 7 Days)', 'time': this.dateoutput, 'message': ret.message},'btnlogs');
                  }
                  if(!schedular && ret.error == 0){
                      console.log(ret.reffId);
                      //store sold download request reff id to local storage
                      setToSyncStorage({ soldDownloadRequestReffId: ret.reffId });
                      this.monitorOrderSyncNow();
                  }
                  resolve(ret);
                };
                request.open('POST', URL_Ebay);

                request.setRequestHeader('Pragma', 'no-cache');
                request.setRequestHeader('Cache-Control', 'no-cache');
                request.setRequestHeader('Upgrade-Insecure-Requests', '1');
                request.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
                request.setRequestHeader('Access-Control-Allow-Origin', '*');
                request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                params.append('EmailAddress', this.email);
                params.append('isChangedView', '1');
                params.append('prevHiddenListingFilter', '4');
                params.append('ListingFilter', '4');
                params.append('DownloadFormatType', '1');
                //params.append('DateRangeType', '3');
                params.append('FEActiveDownloadType', '1');
                params.append('FESoldDownloadType', '1');
                //params.append('DateFromType', '7');
                params.append('DateRangeType', '4');
                params.append('FromMonth', '9');
                params.append('FromDay', '9');
                params.append('FromYear', '2019');
                params.append('FromHour', '0');
                params.append('ToMonth', '9');
                params.append('ToDay', '30');
                params.append('ToYear', '2019');
                params.append('ToHour', '0');
                params.append('srt', srt);

                request.send(params.toString());

            });
        });
    }
    getUploadStatussesfiles(localReffId){
        const URL_Ebay = 'https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeUploadResults';
        const params = new URLSearchParams();
        return new Promise((resolve) => {
            const request = new XMLHttpRequest();
            request.onreadystatechange = (e) => {
                const target = e.currentTarget;
                if (target.readyState !== 4 || target.status !== 200) {
                  return;
                }
                const { responseText } = target;
                const responseURL = target.responseURL;
                try {
                    if (responseURL.includes('//signin.ebay.com')) {
                        var signInText = "<a target='_blank' href='https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeUploadResults'>sign in to your eBay account here</a>";
                        setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Upload Pending Uploads To eBay Monitoring', 'time': this.dateoutput, 'message': 'Please '+signInText+' and then try again'},'btnlogs');
                        resolve ({ error: 1, message: 'Please '+signInText+' and then try again', url: responseURL });
                    }
                    var counter = 0;
                    var obj = this;
                    var innerCounter = 0;
                    $(responseText).find('.BDtC4n').parent('tbody').find('tr').each(function(){
                        if($(this).find('td:nth-child(2)').text().trim() == localReffId){
                            console.log(localReffId); 
                            if($(this).find('td:nth-child(6)').text().trim() == 'Completed'){
                                console.log('hello');
                                var reffId = $(this).find('td:nth-child(2)').text().trim();
                                var responseCsvLink = 'https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeDownload&jobId='+reffId;
                                obj.getFileFromEbayforUploads(responseCsvLink).then((downloadResp) => {
                                    // parse csv and get the result set
                                    console.log(parseCSV(downloadResp));
                                    var csvArray = parseCSV(downloadResp);
                                    var customLabel = '';
                                    var status = '';
                                    var message = '';
                                    var itemId = '';
                                    var apiURL = '';
                                    var columnCount = csvArray[0].length;
                                    for(var i=1;i< csvArray.length;i++){
                                        var itemIdInndex = 7;
                                        var customLabelIndex = 34;
                                        for(var j=0;j< columnCount;j++){
                                            if( csvArray[i][j] != undefined){
                                                if (csvArray[i][j].match(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)) {
                                                    itemIdInndex = j - 3;
                                                    customLabelIndex = j + 24;
                                                    break;
                                                } else {
                                                    
                                                }
                                            }
                                            
                                        }
                                        if(csvArray[i][customLabelIndex])
                                            customLabel = csvArray[i][customLabelIndex];
                                        if(csvArray[i][itemIdInndex])
                                            itemId = csvArray[i][itemIdInndex];

                                        if(csvArray[i][2] == 'Failure'){
                                            status = 'false';
                                            message = csvArray[i][4];
                                        }
                                        else{
                                            status = 'true';
                                            message = 'item is uploaded successfully';
                                        }  
                                        if(customLabel){
                                            obj.sendUploadResponseToAutods(customLabel,status,itemId,message).then((resp) => {
                                                
                                                console.log(resp);
                                                if(resp.error == 1){
                                                   // set button error log for monintoring uploads error with upload reff id and custom label
                                                   setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Upload Pending Uploads To eBay Monitoring', 'time': this.dateoutput, 'message': resp.message},'btnlogs');
                                                }
                                                innerCounter++;
                                                if(innerCounter == ( csvArray.length -1) ){
                                                     
                                                    var productInCsvArray = JSON.parse(localStorage.getItem('uploadedReffIdsTemp'));
                                                    $(productInCsvArray).each(function(index,value){
                                                        if(value == reffId){
                                                            productInCsvArray.splice(index, 1);
                                                            localStorage.setItem('uploadedReffIdsTemp', JSON.stringify(productInCsvArray));
                                                        }
                                                    });
                                                    counter++;
                                                    if(counter == $(responseText).find('.BDtC4n').parent('tbody').find('tr').length){
                                                        console.log('all csv products compared');
                                                        resolve(true);
                                                    }
                                                }
                                            });
                                        }
                                        else{
                                            setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Upload Pending Uploads To eBay Monitoring', 'time': this.dateoutput,'message': "Product skipped to update back in the resopnse file with reff"+localReffId},'btnlogs');
                                            
                                            innerCounter++;
                                            counter++;
                                        }
                                    }
                                    
                                });  
                            }
                            //~ else if($(this).find('td:nth-child(6)').text().trim() == 'Failed'){
                                // retur error msg "Abandoned Job: Abandoning Job: 932087582"
                                    //~ counter++;
                                    //~ if(counter == $(responseText).find('.BDtC4n').parent('tbody').find('tr').length){
                                        //~ console.log('all csvs compared');
                                        //~ resolve(true);
                                    //~ }
                            //~ }
                            else{
                                counter++;
                                if(counter == $(responseText).find('.BDtC4n').parent('tbody').find('tr').length){
                                    console.log('all csv products compared');
                                    resolve(true);
                                }
                            }
                        }
                        else{
                            counter++;
                            if(counter == $(responseText).find('.BDtC4n').parent('tbody').find('tr').length){
                                console.log('all csv products compared');
                                resolve(true);
                            }
                        }
                    });
                    
                } catch (e) {
                  resolve(null);
                }
            };
            request.open('POST', URL_Ebay);
            request.setRequestHeader('Pragma', 'no-cache');
            request.setRequestHeader('Cache-Control', 'no-cache');
            request.setRequestHeader('Upgrade-Insecure-Requests', '1');
            request.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
            request.setRequestHeader('Access-Control-Allow-Origin', '*');
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            params.append('filter', '1');
            request.send(params.toString());
        });
    }
    downloadUploadStatusses() {
        console.log(this.csrftoken);
        $('#message').find('span').text('');
        const URL_Ebay = 'https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeUploadResults';
        const params = new URLSearchParams();
        this.getUpdatedDate();
        this.getUpdatedValidDate();
        return new Promise((resolve) => {
            const request = new XMLHttpRequest();
            request.onreadystatechange = (e) => {
                const target = e.currentTarget;
                if (target.readyState !== 4 || target.status !== 200) {
                  return;
                }
                const { responseText } = target;
                const responseURL = target.responseURL;
                try {
                    if (responseURL.includes('//signin.ebay.com')) {
                        console.log('i am here');
                        var signInText = "<a target='_blank' href='https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeUploadResults'>sign in to your eBay account here</a>";
                        setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Updates Uploads Results To AutoDS', 'time': this.dateoutput, 'message': 'Please '+signInText+' and then try again'},'btnlogs');
                        resolve ({ error: 1, message: 'Please '+signInText+' and then try again', url: responseURL });
                    }
                    var counter = 0;
                    var completed = 0;
                    var obj = this;
                    var innerCounter = 0;
                    $(responseText).find('.BDtC4n').parent('tbody').find('tr').each(function(){
                        if($(this).find('td:nth-child(6)').text().trim() == 'Completed'){
                            var reffId = $(this).find('td:nth-child(2)').text().trim();
                            var responseCsvLink = 'http://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeDownload&jobId='+reffId;
                            obj.getFileFromEbayforUploads(responseCsvLink).then((downloadResp) => {
                                // parse csv and get the result set
                                var csvArray = parseCSV(downloadResp);
                                console.log(csvArray);
                                var customLabel = '';
                                var status = '';
                                var message = '';
                                var itemId = '';
                                var apiURL = '';
                                var columnCount = csvArray[0].length;
                                for(var i=1;i< csvArray.length;i++){
                                    if(csvArray[i][1] == 'Add'){
                                        var itemIdInndex = 7;
                                        var customLabelIndex = 34;
                                        for(var j=0;j< columnCount;j++){
                                            if( csvArray[i][j] != undefined){
                                                if (csvArray[i][j].match(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)) {
                                                    itemIdInndex = j - 3;
                                                    customLabelIndex = j + 24;
                                                    break;
                                                } else {
                                                    
                                                }
                                            }
                                        }
                                        if(csvArray[i][customLabelIndex])
                                            customLabel = csvArray[i][customLabelIndex];
                                        if(csvArray[i][itemIdInndex])
                                            itemId = csvArray[i][itemIdInndex];
                                            
                                        if(csvArray[i][2] == 'Failure'){
                                            status = 'false';
                                            message = csvArray[i][4];
                                        }
                                        else{
                                            status = 'true';
                                            message = 'item is uploaded successfully';
                                        }
                                        if(customLabel){  
                                            obj.sendUploadResponseToAutods(customLabel,status,itemId,message).then((resp) => {
                                                //console.log(resp.message.message); 
                                                if(resp.error == 1){
                                                   // set button error log for monintoring uploads error with upload reff id and custom label
                                                   setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Updates Uploads Results To AutoDS ', 'time': this.dateoutput, 'message': resp.message},'btnlogs');
                                                }
                                                innerCounter++;
                                                if(innerCounter == ( csvArray.length -1) ){
                                                     
                                                    /*var productInCsvArray = JSON.parse(localStorage.getItem('uploadedReffIdsTemp'));
                                                    $(productInCsvArray).each(function(index,value){
                                                        if(value == reffId){
                                                            productInCsvArray.splice(index, 1);
                                                            localStorage.setItem('uploadedReffIdsTemp', JSON.stringify(productInCsvArray));
                                                        }
                                                    });
                                                    counter++;
                                                    if(counter == $(responseText).find('.BDtC4n').parent('tbody').find('tr').length){
                                                        console.log('all csv products compared from upload update button');
                                                        resolve(true);
                                                    }*/
                                                }
                                            });
                                        }
                                        else{
                                            setStatusLogs({ 'logs': true, 'valid_date': this.validdate, 'Button': 'Updates Uploads Results To AutoDS ', 'time': this.dateoutput, 'message': "Product skipped to update back in the resopnse file with reff"+reffId},'btnlogs');
                                            innerCounter++;
                                        }
                                    }
                                    else{
                                        innerCounter++;
                                    }
                                    
                                }
                                console.log(counter);
                                console.log($(responseText).find('.BDtC4n').parent('tbody').find('tr').length);
                                if(counter == $(responseText).find('.BDtC4n').parent('tbody').find('tr').length){
                                    console.log('Successfully updated upload statuses back to autods');
                                    resolve({ error: 0, message: 'Successfully updated upload statuses back to autods'})
                                }
                                
                            }); 
                            completed++; 
                        }
                        counter++;
                    });
                    
                    if(completed == 0){
                        resolve({ error: 1, message: 'No upload status file found with status completed.'})
                    }
                    
                } catch (e) {
                  resolve(null);
                }
            };
            request.open('POST', URL_Ebay);
            request.setRequestHeader('Pragma', 'no-cache');
            request.setRequestHeader('Cache-Control', 'no-cache');
            request.setRequestHeader('Upgrade-Insecure-Requests', '1');
            request.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
            request.setRequestHeader('Access-Control-Allow-Origin', '*');
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            params.append('filter', '1');
            request.send(params.toString());
        });
    }
    downloadLastActiveReadyReport(localReffId = ''){
        const URL_Ebay = 'https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadPickup';
        const formData = new FormData();
        return new Promise((resolve) => {
            const request = new XMLHttpRequest();
            request.onreadystatechange = (e) => {
                const target = e.currentTarget;
                if (target.readyState !== 4 || target.status !== 200) {
                  return;
                }

                const { responseText } = target;
                const responseURL = target.responseURL;
                try {
                    if (responseURL.includes('//signin.ebay.com')) {
                        resolve ({ error: 1, message: 'signIn', url: responseURL });
                    }
                    if(localReffId){
                        // below code is for button;
                        $(responseText).find('.BDtC4n').parent('tbody').find('tr').each(function(){
                            if( $(this).find('td:nth-child(2)').text().trim() == localReffId ){
                                var reffId = $(this).find('td:nth-child(2)').text().trim();
                                if($(this).find('td:nth-child(6)').text().trim() == 'File Complete'){
                                    resolve({ error: 0, message: 'success', reffId: reffId });
                                }
                                else{
                                    if($(this).find('td:nth-child(6)').text().trim() == 'In Progress'){
                                        resolve({ error: 0, message: 'do nothing', reffId: reffId })
                                    }
                                }
                            }
                        });
                    }
                    else{
                        // below code is for schedular
                        $(responseText).find('.BDtC4n').parent('tbody').find('tr').each(function(){
                            if( $(this).find('td:nth-child(4)').text().trim().indexOf('Revise Price and Quantity') != -1){
                                var reffId = $(this).find('td:nth-child(2)').text().trim();
                                if($(this).find('td:nth-child(6)').text().trim() == 'File Complete'){
                                    resolve({ error: 0, message: 'success', reffId: reffId })
                                }
                                else{
                                    if($(this).find('td:nth-child(6)').text().trim() == 'In Progress'){
                                        resolve({ error: 0, message: 'do nothing', reffId: reffId })
                                    }
                                }
                            }
                        });
                    }
                    
                } catch (e) {
                    console.log('i am in exception');
                  resolve(null);
                }
            };
            request.open('POST', URL_Ebay);

            request.setRequestHeader('Pragma', 'no-cache');
            request.setRequestHeader('Cache-Control', 'no-cache');
            request.setRequestHeader('Upgrade-Insecure-Requests', '1');
            request.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
            request.setRequestHeader('Access-Control-Allow-Origin', '*');
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            
            formData.append('filter', '7');
            request.send(formData);

        });
    }
      
    downloadLastSoldReadyReport(localReffId = ''){
        const URL_Ebay = 'https://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SMDownloadPickup';
        const formData = new FormData();
        return new Promise((resolve) => {
            const request = new XMLHttpRequest();
            request.onreadystatechange = (e) => {
                const target = e.currentTarget;
                if (target.readyState !== 4 || target.status !== 200) {
                  return;
                }

                const { responseText } = target;
                const responseURL = target.responseURL;
                try {
                    if (responseURL.includes('//signin.ebay.com')) {
                        resolve ({ error: 1, message: 'signIn', url: responseURL });
                    }
                    if(localReffId){
                        // below code is for button;
                        $(responseText).find('.BDtC4n').parent('tbody').find('tr').each(function(){
                            if( $(this).find('td:nth-child(2)').text().trim() == localReffId ){
                                var reffId = $(this).find('td:nth-child(2)').text().trim();
                                if($(this).find('td:nth-child(6)').text().trim() == 'File Complete'){
                                    resolve({ error: 0, message: 'success', reffId: reffId });
                                }
                                else{
                                    if($(this).find('td:nth-child(6)').text().trim() == 'In Progress'){
                                        resolve({ error: 0, message: 'do nothing', reffId: reffId })
                                    }
                                }
                            }
                        });
                    }
                    else{
                        $(responseText).find('.BDtC4n').parent('tbody').find('tr').each(function(){
                            if( $(this).find('td:nth-child(4)').text().trim() == 'Sold'){
                                var reffId = $(this).find('td:nth-child(2)').text().trim();
                                if($(this).find('td:nth-child(6)').text().trim() == 'File Complete'){
                                    resolve({ error: 0, message: 'success', reffId: reffId })
                                }
                                else{
                                    if($(this).find('td:nth-child(6)').text().trim() == 'In Progress'){
                                        resolve({ error: 0, message: 'do nothing', reffId: reffId })
                                    }
                                }
                            }
                        });
                    }
                } catch (e) {
                  resolve(null);
                }
            };
            request.open('POST', URL_Ebay);

            request.setRequestHeader('Pragma', 'no-cache');
            request.setRequestHeader('Cache-Control', 'no-cache');
            request.setRequestHeader('Upgrade-Insecure-Requests', '1');
            request.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
            request.setRequestHeader('Access-Control-Allow-Origin', '*');
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            
            formData.append('filter', '7');
            request.send(formData);

        });
    }  
    
}
