var API_URL = "https://app.autods.com/";
chrome.runtime.setUninstallURL('http://autods.com/non_api_extension_uninstall');
function setToSyncStorage(data) {
  return new Promise((resolve) => {
    chrome.storage.sync.set(data, resolve);
  })
}

function setStatusLogs(data,type){
    return new Promise((resolve) => {
        var alllogs = [];
        if(localStorage.getItem(type)){
            var getprevious = JSON.parse(localStorage.getItem(type));
            if(getprevious.length !=0){
                for (var i = 0; i < getprevious.length; i++) {
                    alllogs.push(getprevious[i]);
                }
            }
        }
        alllogs.push(data);
        localStorage.setItem(type, JSON.stringify(alllogs));
    })
}

function getStatusLogs(type){
       return new Promise((resolve) => {
      resolve(JSON.parse(localStorage.getItem(type)));
  })
}

function getFromSyncStorage(keys) {
  return new Promise((resolve) => {
    chrome.storage.sync.get(keys, (data) => {
      if (Array.isArray(keys)) {
        resolve(data)
      } else {
        resolve(data[keys])
      }
    });
  })
}
function getLatestCrfToken(){
    return new Promise(resolve => {
        chrome.cookies.get({'url': API_URL, 'name': 'csrftoken'}, function(cookie) {
            if(cookie){
                resolve(cookie.value)
            }
        }); 
    });
}

function parseCSV(str) {
    var arr = [];
    var quote = false;  // true means we're inside a quoted field

    // iterate over each character, keep track of current row and column (of the returned array)
    for (var row = 0, col = 0, c = 0; c < str.length; c++) {
        var cc = str[c], nc = str[c+1];        // current character, next character
        arr[row] = arr[row] || [];             // create a new row if necessary
        arr[row][col] = arr[row][col] || '';   // create a new column (start with empty string) if necessary

        // If the current character is a quotation mark, and we're inside a
        // quoted field, and the next character is also a quotation mark,
        // add a quotation mark to the current column and skip the next character
        if (cc == '"' && quote && nc == '"') { arr[row][col] += cc; ++c; continue; }  

        // If it's just one quotation mark, begin/end quoted field
        if (cc == '"') { quote = !quote; continue; }

        // If it's a comma and we're not in a quoted field, move on to the next column
        if (cc == ',' && !quote) { ++col; continue; }

        // If it's a newline (CRLF) and we're not in a quoted field, skip the next character
        // and move on to the next row and move to column 0 of that new row
        if (cc == '\r' && nc == '\n' && !quote) { ++row; col = 0; ++c; continue; }

        // If it's a newline (LF or CR) and we're not in a quoted field,
        // move on to the next row and move to column 0 of that new row
        if (cc == '\n' && !quote) { ++row; col = 0; continue; }
        if (cc == '\r' && !quote) { ++row; col = 0; continue; }

        // Otherwise, append the current character to the current column
        arr[row][col] += cc;
    }
    return arr;
}
const keys = [
    'token',
    'email',
    'startActiveScheduleSync',
    'startSoldScheduleSync',
    'startuploadToEbayScheduleSync',
    'monitorUploadStatuses',
    'lastActiveDownloadTime',
    'lastSoldDownloadTime',
    'lastUploadTime',
    'activeDownloadRequestReffId',
    'soldDownloadRequestReffId'
];
getFromSyncStorage(keys).then((data) => {
    if( (data.email) && (data.token)){
        EbaytoAutoDS = new EbaytoAutoDS(data.email,data.token);
        getLatestCrfToken().then((resp) =>{
            EbaytoAutoDS.csrftoken = resp;
            EbaytoAutoDS.email = data.email;
            EbaytoAutoDS.token = data.token;
            EbaytoAutoDS.monitorUploadStatuses();
        }); 
        if(data.activeDownloadRequestReffId){
            EbaytoAutoDS.monitorUntrackedSyncNow();
        }
        if(data.soldDownloadRequestReffId){
            EbaytoAutoDS.monitorOrderSyncNow();
        }
        if(data.startActiveScheduleSync){
            if(data.lastActiveDownloadTime){
                var timeGap = Math.floor( (Date.now() - data.lastActiveDownloadTime)); // time gap in milliseconds seconds
                //console.log(timeGap); 
                if(timeGap > 3600000 ){ // last schedular run and current time gap exceed the 60 minutes is 3600000 milliseconds seconds
                    EbaytoAutoDS.startActiveScheduleSync('runNow');
                }
                else{
                    var timeRemaining = 3600000 - timeGap;
                    var timeInMinutes = timeRemaining/1000/60;
                    console.log('Active schedular will run after '+timeInMinutes.toFixed(0)+' minutes');
                    EbaytoAutoDS.startActiveScheduleSync(timeRemaining);
                }
            }
            else{
                EbaytoAutoDS.startActiveScheduleSync('runNow');
            }
        }
        else{
            EbaytoAutoDS.stopActiveScheduleSync();
        }
        
        if(data.startSoldScheduleSync){
            if(data.lastSoldDownloadTime){
                var timeGap = Math.floor( (Date.now() - data.lastSoldDownloadTime)); // time gap in milliseconds seconds
                if(timeGap > 1200000 ){ // last schedular run and current time gap exceed the 20 minutes is 1200000 milliseconds seconds
                    EbaytoAutoDS.startSoldScheduleSync('runNow')
                }
                else{
                    var timeRemaining = 1200000 - timeGap;
                    var timeInMinutes = timeRemaining/1000/60;
                    console.log('Sold schedular will run after '+timeInMinutes.toFixed(0)+' minutes');
                    EbaytoAutoDS.startSoldScheduleSync(timeRemaining)
                }
            }
            else{
                EbaytoAutoDS.startSoldScheduleSync('runNow')
            }
        }
        else{
            EbaytoAutoDS.stopSoldScheduleSync();
        }
        if(data.startuploadToEbayScheduleSync){
            if(data.lastUploadTime){
                var timeGap = Math.floor( (Date.now() - data.lastUploadTime)); // time gap in milliseconds seconds
                //console.log(timeGap);
                if(timeGap > 3600000 ){ // last schedular run and current time gap exceed the 60 minutes is 3600000 milliseconds seconds
                    EbaytoAutoDS.startuploadToEbayScheduleSync('runNow')
                }
                else{
                    var timeRemaining = 3600000 - timeGap;
                    var timeInMinutes = timeRemaining/1000/60;
                    console.log('Repricer schedular will run after '+timeInMinutes.toFixed(0)+' minutes');
                    EbaytoAutoDS.startuploadToEbayScheduleSync(timeRemaining)
                }
            }
            else{
                EbaytoAutoDS.startuploadToEbayScheduleSync('runNow')
            }
        }
        else{
            EbaytoAutoDS.stopuploadToEbayScheduleSync();
        }
        
        if( (data.startActiveScheduleSync) && (data.startSoldScheduleSync) && (data.startuploadToEbayScheduleSync) ){
            chrome.browserAction.setBadgeText ( { text: "" } );
        }
        else{
            chrome.browserAction.setBadgeBackgroundColor({ color: '#dcaa70' });
            chrome.browserAction.setBadgeText ( { text: "Stop" } );
        }

        
    }
    else{
       EbaytoAutoDS = new EbaytoAutoDS('',''); 
       chrome.browserAction.setBadgeBackgroundColor({ color: '#dcaa70' });
       chrome.browserAction.setBadgeText ( { text: "Stop" } );
    }
}); 

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    
        if (msg.callback) {
            msg.sender = sender;
            chrome.tabs.sendMessage(sender.tab.id, msg);
        }
        else switch (msg.action) {
            case 'images':
                console.log('images images')
                /*let promises = [
                    /*new Promise((resolve, reject) => {
                        $.ajax({
                            type: "POST", contentType: "application/json; charset=utf-8", dataType: "json",
                            url: domain + "Service/UploadWebService.asmx/uploadImages",
                            data: JSON.stringify({ srcImages: request.images, email: request.email }),
                            success: response => resolve(response.d), failure: () => reject(),
                        })
                    }),
                    new Promise((resolve, reject) => {
                        $.ajax({
                            type: "POST", contentType: "application/json; charset=utf-8", dataType: "json",
                            url: domain + "Service/UploadWebService.asmx/uploadImagesVariation",
                            data: JSON.stringify({ srcImages: request.variations }),
                            success: response => resolve(response.d), failure: () => reject(),
                        });
                    })
                ]; 
                Promise.all(promises).then(result => sendResponse({ images: request.images, variations: request.variations })); */
                return true;
            case 'importEbay':
                console.log('importEbay')
                var suffix = msg.country;
                chrome.tabs.create(
                    { active: true, url: 'https://bulksell.ebay.' + suffix + '/ws/eBayISAPI.dll?SingleList&sellingMode=AddItem' },
                    tab => chrome.tabs.executeScript(tab.id, { code: "var runImport = true;", runAt: 'document_start' })
                );
                break; 
            case 'validateToken':
                EbaytoAutoDS.validateToken(msg.token).then(sendResponse);        
                return true;
            case 'uploadToEbay':
                //~ chrome.browserAction.setIcon({
                    //~ path: "../images/icon.png"
                //~ });
                //~ chrome.browserAction.setBadgeBackgroundColor({ color: '#dcaa70' });
                //~ chrome.browserAction.setBadgeText ( { text: "Error" } );
                getFromSyncStorage(keys).then((data) => {
                    if( (data.email) && (data.token)){
                        //getLatestCrfToken().then((resp) =>{
                            //EbaytoAutoDS.csrftoken = resp;
                            EbaytoAutoDS.email = data.email;
                            EbaytoAutoDS.token = data.token;
                            EbaytoAutoDS.uploadToEbayZip().then(sendResponse);
                        //});
                    }
                });
                return true;
            case 'downloadActiveReport':
                getFromSyncStorage(keys).then((data) => {
                    if( (data.email) && (data.token)){
                        //getLatestCrfToken().then((resp) =>{
                            //EbaytoAutoDS.csrftoken = resp;
                            EbaytoAutoDS.email = data.email;
                            EbaytoAutoDS.token = data.token;
                            EbaytoAutoDS.downloadActiveReport().then(sendResponse);
                        //});
                    }
                });
                return true;
            case 'downloadSoldReport':
                getFromSyncStorage(keys).then((data) => {
                    if( (data.email) && (data.token)){
                        //getLatestCrfToken().then((resp) =>{
                            //EbaytoAutoDS.csrftoken = resp;
                            EbaytoAutoDS.email = data.email;
                            EbaytoAutoDS.token = data.token;
                            EbaytoAutoDS.downloadSoldReport().then(sendResponse);
                        //}); 
                    }
                });
                return true;
            case 'downloadUploadStatusses':
                getFromSyncStorage(keys).then((data) => {
                    if( (data.email) && (data.token)){
                        //getLatestCrfToken().then((resp) =>{
                            //EbaytoAutoDS.csrftoken = resp;
                            EbaytoAutoDS.email = data.email;
                            EbaytoAutoDS.token = data.token;
                            EbaytoAutoDS.downloadUploadStatusses().then(sendResponse);
                        //});  
                    }
                });
                return true;
            case 'startActiveScheduleSync':
                getFromSyncStorage(keys).then((data) => {
                    if( (data.email) && (data.token)){
                        getLatestCrfToken().then((resp) =>{
                            EbaytoAutoDS.csrftoken = resp;
                            EbaytoAutoDS.email = data.email;
                            EbaytoAutoDS.token = data.token;
                            EbaytoAutoDS.startActiveScheduleSync(msg.param).then(sendResponse);
                        });  
                    }
                });
                return true;
            case 'startSoldScheduleSync':
                getFromSyncStorage(keys).then((data) => {
                    if( (data.email) && (data.token)){
                        getLatestCrfToken().then((resp) =>{
                            EbaytoAutoDS.csrftoken = resp;
                            EbaytoAutoDS.email = data.email;
                            EbaytoAutoDS.token = data.token;
                            EbaytoAutoDS.startSoldScheduleSync(msg.param).then(sendResponse);
                        }); 
                    }
                });
                return true;
            case 'startuploadToEbayScheduleSync':
                getFromSyncStorage(keys).then((data) => {
                    if( (data.email) && (data.token)){
                        getLatestCrfToken().then((resp) =>{
                            EbaytoAutoDS.csrftoken = resp;
                            EbaytoAutoDS.email = data.email;
                            EbaytoAutoDS.token = data.token;
                            EbaytoAutoDS.startuploadToEbayScheduleSync(msg.param).then(sendResponse);
                        });
                    }
                });
                return true;
            case 'monitorUploadStatuses':
                getFromSyncStorage(keys).then((data) => {
                    if( (data.email) && (data.token)){
                        getLatestCrfToken().then((resp) =>{
                            EbaytoAutoDS.csrftoken = resp;
                            EbaytoAutoDS.email = data.email;
                            EbaytoAutoDS.token = data.token;
                            EbaytoAutoDS.monitorUploadStatuses().then(sendResponse);
                        });
                    }
                });
                return true;
            case 'stopActiveScheduleSync':
                getFromSyncStorage(keys).then((data) => {
                    if( (data.email) && (data.token)){
                        //getLatestCrfToken().then((resp) =>{
                            //EbaytoAutoDS.csrftoken = resp;
                            EbaytoAutoDS.email = data.email;
                            EbaytoAutoDS.token = data.token;
                            EbaytoAutoDS.stopActiveScheduleSync().then(sendResponse);
                        //});  
                    }
                });
                return true;
            case 'stopSoldScheduleSync':
                getFromSyncStorage(keys).then((data) => {
                    if( (data.email) && (data.token)){
                        //getLatestCrfToken().then((resp) =>{
                            //EbaytoAutoDS.csrftoken = resp;
                            EbaytoAutoDS.email = data.email;
                            EbaytoAutoDS.token = data.token;
                            EbaytoAutoDS.stopSoldScheduleSync().then(sendResponse);
                        //});  
                    }
                });
                return true;
            case 'stopuploadToEbayScheduleSync':
                getFromSyncStorage(keys).then((data) => {
                    if( (data.email) && (data.token)){
                        //getLatestCrfToken().then((resp) =>{
                            //EbaytoAutoDS.csrftoken = resp;
                            EbaytoAutoDS.email = data.email;
                            EbaytoAutoDS.token = data.token;
                            EbaytoAutoDS.stopuploadToEbayScheduleSync().then(sendResponse);
                        //});   
                    }
                });
                return true;
            
            default:
                sendResponse('unknown message');
                break;
        }

   
});

