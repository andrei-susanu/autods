function setToSyncStorage(data) {
  return new Promise((resolve) => {
    chrome.storage.sync.set(data, resolve);
  })
}
function getFromSyncStorage(keys) {
  return new Promise((resolve) => {
    chrome.storage.sync.get(keys, (data) => {
      if (Array.isArray(keys)) {
        resolve(data)
      } else {
        resolve(data[keys])
      }
    });
  })
}
function checkStorageCredentials(){
    return new Promise((resolve) => {
        const keys = [
            'token',
            'email',
        ];
        getFromSyncStorage(keys).then((data) => {
            if( (!data.email) || (!data.token)){
                resolve(false);
            }
            else{
                resolve(true);
            }
        });  
    });
}
function startSoldSchedularMsgs(){
    $('#soldReportStatus').text('Running');
    $("#soldReportStatus").removeClass();
    $('#soldReportStatus').addClass('green-text');
    $('#soldSchedulerStop').show();
    $('#soldScheduler').hide();  
}
function stopSoldSchedularMsgs(){
    $('#soldReportStatus').text('Stopped');
    $("#soldReportStatus").removeClass();
    $('#soldReportStatus').addClass('red-text');
    $('#soldSchedulerStop').hide();
    $('#soldScheduler').show();   
}
function startActiveSchedularMsgs(){
    $('#actReportStatus').text('Running');
    $("#actReportStatus").removeClass();
    $('#actReportStatus').addClass('green-text'); 
    $('#activeSchedulerStop').show();
    $('#activeScheduler').hide(); 
}
function stopActiveSchedularMsgs(){
    $('#actReportStatus').text('Stopped');
    $("#actReportStatus").removeClass();
    $('#actReportStatus').addClass('red-text');  
    $('#activeSchedulerStop').hide();
    $('#activeScheduler').show(); 
}
function startUploadToEbaySchedularMsgs(){
    $('#uploadEbayStatus').text('Running');
    $("#uploadEbayStatus").removeClass();
    $('#uploadEbayStatus').addClass('green-text'); 
    $('#uploadEbaySchedulerStop').show();
    $('#uploadEbayScheduler').hide(); 
}
function stopUploadToEbaySchedularMsgs(){
    $('#uploadEbayStatus').text('Stopped');
    $("#uploadEbayStatus").removeClass();
    $('#uploadEbayStatus').addClass('red-text');  
    $('#uploadEbaySchedulerStop').hide();
    $('#uploadEbayScheduler').show(); 
}
