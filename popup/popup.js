$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
class Main {
  constructor() {
    this.credentials = {};
    this.config = {};
    window.onload = () => this.initInterface()
  }
  getCredentials() {
    return new Promise((resolve) => {
      chrome.runtime.sendMessage({action: 'get_credentials'}, (response) => {
        Object.assign(this.credentials, response);

        resolve();
      });
    });
  }

getlognotification(){
var counter = 0;
if(localStorage.getItem('logs') != null || localStorage.getItem('btnlogs') != null){
  var allchklogs = JSON.parse(localStorage.getItem('logs'));
  var allchkbtnlogs = JSON.parse(localStorage.getItem('btnlogs'));
    console.log(allchklogs);
   if((allchklogs != null) && (allchklogs.length !=0)){
     for (var i = 0; i < allchklogs.length; i++) {
            if(allchklogs[i].logs == true){
                    counter ++;
          }
        }
    }
    if((allchkbtnlogs != null) && (allchkbtnlogs.length !=0)){
     for (var i = 0; i < allchkbtnlogs.length; i++) {
            if(allchkbtnlogs[i].logs == true){
                    counter ++;
          }
        }
    }
    console.log(counter);
    if(counter > 0){
        $('#log_noticount').show();
        $('#log_noticount').html(counter);
    }
    else{
        $('#log_noticount').hide();
    }

  }
  else{
    //$('#logstatus').hide();
    $('#log_noticount').hide();
  }

}
  initView() {
    this.view = new View(this.credentials, this.config, this.logs);
  }
  isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
  }
  checkCredentials(){
    return new Promise(resolve => {
        if( (!document.getElementById('email').value) || (!document.getElementById('remote_key').value) ){
            $('#message').find('span').html('Please fill the both fields');
            resolve(false);
        }
        else if(!this.isValidEmailAddress(document.getElementById('email').value)){
            document.getElementById('email').value = '';
            $('#message').find('span').html('Please enter valid email');
            resolve(false);
        }
        else {
            $('#loading').show();
            chrome.runtime.sendMessage({action: 'validateToken', token: document.getElementById('remote_key').value}, (response) => {
                console.log(response);
                $('#loading').hide();
                if(response.status == 'error'){
                    $('#message').find('span').html(response.message);
                    resolve(false);
                }
                resolve(true);
            });
        }
        
    });
  }
  initInterface() {

     //notifications
    this.getlognotification();
    //end here

    const keys = [
        'token',
        'email',
        'startSoldScheduleSync',
        'startActiveScheduleSync',
        'startuploadToEbayScheduleSync',
    ];
    getFromSyncStorage(keys).then((data) => {
        console.log(data);
        if( (data.email) && (data.token)){
            document.getElementById('email').value = data.email;
            document.getElementById('remote_key').value = data.token;
            if(data.startActiveScheduleSync){
                startActiveSchedularMsgs();
            }
            else{
                stopActiveSchedularMsgs();
            }  
            if(data.startSoldScheduleSync){
                startSoldSchedularMsgs();
            }
            else{
                stopSoldSchedularMsgs();
            }
            if(data.startuploadToEbayScheduleSync){
                startUploadToEbaySchedularMsgs();
            }
            else{
                stopUploadToEbaySchedularMsgs();
            }
        }
        else{
            stopActiveSchedularMsgs(); 
            stopSoldSchedularMsgs();
            stopUploadToEbaySchedularMsgs();
        }
    });  
    
    //log button click
     document
      .getElementById('logstatus')
      .addEventListener('click', () => {
        chrome.tabs.create({url: chrome.extension.getURL('log.html')});
    });
      //end here
    document
    .getElementById('save_settings')
    .addEventListener('click', () => {
        $('#message').find('span').html('');
        this.checkCredentials().then((resp) => {
            if(resp){
                // save settings to local storage
                setToSyncStorage({ token: document.getElementById('remote_key').value });
                setToSyncStorage({ email: document.getElementById('email').value });
                $('#message').find('span').html('Settings saved successfully');
                // check its settings saved first time or user is updating the toke and start schedulaer accordingly
                const keys = [
                    'startActiveScheduleSync',
                    'startSoldScheduleSync',
                    'startuploadToEbayScheduleSync',
                ];
                getFromSyncStorage(keys).then((data) => {
                    if(!data.startSoldScheduleSync)
                        chrome.runtime.sendMessage({action: 'startSoldScheduleSync', param: 'runNow'}, (response) => { console.log('sold schedular called'); });
                    if(!data.startActiveScheduleSync)
                        chrome.runtime.sendMessage({action: 'startActiveScheduleSync', param: 'runNow'}, (response) => { console.log('active schedular called'); });
                    if(!data.startuploadToEbayScheduleSync)
                        chrome.runtime.sendMessage({action: 'startuploadToEbayScheduleSync', param: 'runNow'}, (response) => { console.log('upload schedular called'); });
                });        
                chrome.runtime.sendMessage({action: 'monitorUploadStatuses'}, (response) => { console.log('Monitor upload statuses called'); });
            }
        });
    });
    
    document
    .getElementById('uploadToEbay')
    .addEventListener('click', () => {
      $('#message').find('span').html('');
      checkStorageCredentials().then((resp) => {
        if(resp){
            this.checkCredentials().then((resp1) => {
                if(resp1){
                    $('#loading').show();
                    chrome.runtime.sendMessage({action: 'uploadToEbay'}, (response) => {
                        if(response){
                            $('#loading').hide();
                            this.getlognotification();
                            console.log(response);
                            if(response.error == 1){
                                $('#message').find('span').html(response.message);
                            }
                            else{
                                $('#message').find('span').html(response.message);
                            }
                        }
                        
                    });
                }
            });
        }else{
            $('#message').find('span').html('Please save the valid email and token first to proceed further');  
        }
      });
    });
    document
    .getElementById('download1')
    .addEventListener('click', () => {
      $('#message').find('span').html('');
      checkStorageCredentials().then((resp) => {
        if(resp){
            this.checkCredentials().then((resp1) => {
                if(resp1){
                    $('#loading').show();
                    chrome.runtime.sendMessage({action: 'downloadActiveReport'}, (response) => {
                        if(response){
                            $('#loading').hide();
                            this.getlognotification();
                            if(response.error == 1){
                                $('#message').find('span').html(response.message);
                            }
                            else{
                                $('#message').find('span').html(response.message);
                               
                            }
                        }
                        
                    });
                }
            });
        }else{
            $('#message').find('span').html('Please save the valid email and token first to proceed further');  
        }
      });
    });
    document
      .getElementById('download2')
      .addEventListener('click', () => {
        $('#message').find('span').html('');
        checkStorageCredentials().then((resp) => {
          if(resp){
            this.checkCredentials().then((resp1) => {
                if(resp1){
                  $('#loading').show();
                  chrome.runtime.sendMessage({action: 'downloadSoldReport'}, (response) => {
                        if(response){
                            $('#loading').hide();
                            this.getlognotification();
                            if(response.error == 1){
                                $('#message').find('span').html(response.message);
                            }
                            else{
                                $('#message').find('span').html(response.message);
                                
                            }
                        }
                  });
                }
            });
          }
          else{
              $('#message').find('span').html('Please save the valid email and token first to proceed further'); 
          }
        });

    });
    document
      .getElementById('download3')
      .addEventListener('click', () => {
        $('#message').find('span').html('');
        checkStorageCredentials().then((resp) => {
          if(resp){
            this.checkCredentials().then((resp1) => {
                if(resp1){
                  $('#loading').show();
                  chrome.runtime.sendMessage({action: 'downloadUploadStatusses'}, (response) => {
                        if(response){
                            $('#loading').hide();
                            console.log(response);
                            this.getlognotification();
                            if(response.error == 1){
                                console.log('test');
                                $('#message').find('span').html(response.message);
                            }
                            else{
                                $('#message').find('span').html(response.message);
                            }
                        }
                  });
                }
            });
          }
          else{
              $('#message').find('span').html('Please save the valid email and token first to proceed further'); 
          }
        });
    });
    document
      .getElementById('soldScheduler')
      .addEventListener('click', () => {
        $('#message').find('span').html('');
        checkStorageCredentials().then((resp) => {
          if(resp){
            //this.checkCredentials().then((resp1) => {
                //if(resp1){
                    const keys = [
                        'startSoldScheduleSync',
                        'lastSoldDownloadTime'
                    ];
                    $(this).hide();
                    $('#soldSchedulerStop').show();
                    var parameter = 'runNow';
                    getFromSyncStorage(keys).then((data) => {
                        if(data.lastSoldDownloadTime){
                            var timeGap = Math.floor( (Date.now() - data.lastSoldDownloadTime)); // time gap in milliseconds seconds
                            if(timeGap < 1200000 ){ // last schedular run and current time gap exceed the 20 minutes is 1200000 milliseconds seconds
                                var timeRemaining = 1200000 - timeGap;
                                var timeInMinutes = timeRemaining/1000/60;
                                console.log('Sold schedular will run after '+timeInMinutes.toFixed(0)+' minutes');
                                parameter = timeRemaining;
                            }
                        }   
                        if(data.startSoldScheduleSync){
                            //$('#message').find('span').text('Scedular for Sold report is already running'); 
                            startSoldSchedularMsgs();
                        }
                        else{
                            $('#message').find('span').html('Scheduler started for sold report');
                            startSoldSchedularMsgs();
                            chrome.runtime.sendMessage({action: 'startSoldScheduleSync', param: parameter}, (response) => {
                                console.log('sold resp');
                                 //notifications
                                  this.getlognotification();
                                  //end here
                                console.log(response);
                                if(response){
                                    if(response.error == 1){
                                        $('#message').find('span').html(response.message);
                                        //stopSoldSchedularMsgs();
                                    }
                                    //~ if(response.message == 'signin'){
                                        //~ $('#message').find('span').text('Please sign in to ebay first');
                                        //~ stopSoldSchedularMsgs();
                                    //~ }
                                    //~ if(response.message == 'success'){
                                        //~ $('#message').find('span').text('Scheduler started for sold report');
                                    //~ }
                                }
                            });
                        }
                    });  
                //}
            //});
              
          }
          else{
              $('#message').find('span').html('Please save the valid email and token first to proceed further'); 
              stopSoldSchedularMsgs();
          }
        });
    });
    document
      .getElementById('soldSchedulerStop')
      .addEventListener('click', () => {
        $('#message').find('span').html('');
        checkStorageCredentials().then((resp) => {
          if(resp){
              $(this).hide();
              $('#soldScheduler').show();
              chrome.runtime.sendMessage({action: 'stopSoldScheduleSync'}, (response) => {
                    if(response){
                        if(response.message == 'stop'){
                            $('#message').find('span').html('Sold Report Scheduler stoped');
                            stopSoldSchedularMsgs();
                        }
                        if(response.message == 'already_stopped'){
                            //$('#message').find('span').text('Already stopped');
                            stopSoldSchedularMsgs();
                        }
                    }
              });
          }
          else{
              $('#message').find('span').html('Please save the valid email and token first to proceed further'); 
          }
        });
    });
    document
      .getElementById('activeScheduler')
      .addEventListener('click', () => {
        
        $('#message').find('span').html('');
        checkStorageCredentials().then((resp) => {
          if(resp){
            //this.checkCredentials().then((resp1) => {
                //if(resp1){
                  $(this).hide();
                  $('#activeSchedulerStop').show();
                  const keys = [
                        'startActiveScheduleSync',
                        'lastActiveDownloadTime'
                  ];
                  var parameter = 'runNow';
                  getFromSyncStorage(keys).then((data) => {
                    if(data.lastActiveDownloadTime){
                        var timeGap = Math.floor( (Date.now() - data.lastActiveDownloadTime)); // time gap in milliseconds seconds
                        if(timeGap < 3600000 ){ // last schedular run and current time gap exceed the 60 minutes is 3600000 milliseconds seconds
                            var timeRemaining = 3600000 - timeGap;
                            var timeInMinutes = timeRemaining/1000/60;
                            console.log('Active schedular will run after '+timeInMinutes.toFixed(0)+' minutes');
                            parameter = timeRemaining;
                        }
                    }
                    if(data.startActiveScheduleSync){
                        //$('#message').find('span').text('Scedular for Active report is already running'); 
                        startActiveSchedularMsgs();
                    }
                    else{
                        $('#message').find('span').html('Scheduler started for Active listing report');
                        startActiveSchedularMsgs();
                        chrome.runtime.sendMessage({action: 'startActiveScheduleSync', param: parameter}, (response) => {
                            this.getlognotification();
                            if(response){
                                if(response.error == 1){
                                    $('#message').find('span').html(response.message);
                                    //stopActiveSchedularMsgs();
                                }
                                //~ if(response.message == 'signin'){
                                    //~ $('#message').find('span').text('Please sign in to ebay first');
                                    //~ stopActiveSchedularMsgs();
                                //~ }
                                //~ if(response.message == 'success'){
                                    //~ $('#message').find('span').text('Scheduler started for Active listing report');
                                //~ }
                            }
                        });
                    }
                  }); 
                //}
            //});
          }
          else{
              $('#message').find('span').html('Please save the valid email and token first to proceed further'); 
              stopActiveSchedularMsgs();
          }
        });
    });
    document
      .getElementById('activeSchedulerStop')
      .addEventListener('click', () => {
        $('#message').find('span').html('');
        checkStorageCredentials().then((resp) => {
          if(resp){
              console.log('i am here');
              $(this).hide();
              $('#activeScheduler').show();
              
              chrome.runtime.sendMessage({action: 'stopActiveScheduleSync'}, (response) => {
                  console.log(response);
                    if(response){
                        if(response.message == 'stop'){
                            $('#message').find('span').html('Active listing Report Scheduler stoped');
                            stopActiveSchedularMsgs();
                        }
                        if(response.message == 'already_stopped'){
                            //$('#message').find('span').text('Already stopped');
                            stopActiveSchedularMsgs();
                        }
                    }
              });
          }
          else{
              $('#message').find('span').html('Please save the valid email and token first to proceed further'); 
          }
        });
    });
    document
      .getElementById('uploadEbayScheduler')
      .addEventListener('click', () => {
        
        $('#message').find('span').html('');
        checkStorageCredentials().then((resp) => {
          if(resp){
              $(this).hide();
              $('#uploadEbaySchedulerStop').show();
              const keys = [
                    'startuploadToEbayScheduleSync',
                    'lastUploadTime'
              ];
              var parameter = 'runNow';
              getFromSyncStorage(keys).then((data) => {
                if(data.lastUploadTime){
                    var timeGap = Math.floor( (Date.now() - data.lastUploadTime)); // time gap in milliseconds seconds
                    if(timeGap < 3600000 ){ // last schedular run and current time gap exceed the 60 minutes is 3600000 milliseconds seconds
                        var timeRemaining = 3600000 - timeGap;
                        var timeInMinutes = timeRemaining/1000/60;
                        console.log('Repricer schedular will run after '+timeInMinutes.toFixed(0)+' minutes');
                        parameter = timeRemaining;
                    }
                }
                if(data.startuploadToEbayScheduleSync){
                    //$('#message').find('span').html('Scedular for Active report is already running'); 
                    startUploadToEbaySchedularMsgs();
                }
                else{
                    $('#message').find('span').html('Scheduler started for Upload to Ebay');
                    startUploadToEbaySchedularMsgs();
                    chrome.runtime.sendMessage({action: 'startuploadToEbayScheduleSync', param: parameter}, (response) => {
                        this.getlognotification();
                        if(response){
                            if(response.error == 1){
                                $('#message').find('span').html(response.message);
                            }
                        }
                    });
                }
              }); 
          }
          else{
              $('#message').find('span').html('Please save the valid email and token first to proceed further'); 
              stopUploadToEbaySchedularMsgs();
          }
        });
    });
    document
      .getElementById('uploadEbaySchedulerStop')
      .addEventListener('click', () => {
        $('#message').find('span').html('');
        checkStorageCredentials().then((resp) => {
          if(resp){
              $(this).hide();
              $('#uploadEbayScheduler').show();
              
              chrome.runtime.sendMessage({action: 'stopuploadToEbayScheduleSync'}, (response) => {
                    if(response){
                        if(response.message == 'stop'){
                            $('#message').find('span').html('Upload to Ebay Scheduler stoped');
                            stopUploadToEbaySchedularMsgs();
                        }
                        if(response.message == 'already_stopped'){
                            //$('#message').find('span').html('Already stopped');
                            stopUploadToEbaySchedularMsgs();
                        }
                    }
              });
          }
          else{
              $('#message').find('span').html('Please save the valid email and token first to proceed further'); 
          }
        });
    });
  }
}

window.inst = new Main();
