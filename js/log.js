let getlogs = localStorage.getItem('logs');
let getlogsbtn = localStorage.getItem('btnlogs');
let alllogs = []; 
let alllogsbtn = []; 
//console.log(getlogs);
var table1;
if(getlogs != null ){
		if(getlogs.length !=0){
	 	   var getdata = JSON.parse(getlogs);
	      //console.log('With outseen='+getdata); 
        for (var i = 0; i < getdata.length; i++) {
              getdata[i].logs = false;
              alllogs.push(getdata[i]);         
        }
        // console.log("updated seen "+getdata);  
      localStorage.setItem('logs', JSON.stringify(alllogs));
        //console.log(localStorage.getItem('logs'));             
      table1 = $('#example').DataTable ({
          //"data" : getdata.reverse(),
          "ordering": false,
          "data" : getdata,
          //"order": [ 2, "desc" ],
          //"ordering": true,
          "columns" : [
              { "data" : "Schedular",'title' : 'Schedular' },
              { "data" : "message",'title' : 'Message'  },
              { "data" : "time",'title' : 'Time'  },
              {
                data: null,
                className: "center",
                defaultContent: '<a href="" class="editor_remove">Clear</a>'
              }
          ]
      });

     // Delete a record
     $('#example').on('click', 'a.editor_remove', function (e) {
        e.preventDefault();
        console.log($(this).closest('tr'));
        var rowIndex = $(this).closest('tr')[0]['sectionRowIndex'];
        $(this).closest('tr').remove();
        console.log(getdata);
        let getlogs = localStorage.getItem('logs');
        var getdata = JSON.parse(getlogs);
        var temLogs = [...getdata];
        temLogs.sort(function(a, b){return b.valid_date - a.valid_date});
        temLogs.splice(rowIndex, 1); 
        var temLogsNew = [...temLogs];
        temLogsNew.sort(function(a, b){return a.valid_date - b.valid_date});
        console.log(temLogsNew);
        localStorage.setItem('logs', JSON.stringify(temLogsNew)); 
        //table1.ajax.reload();
     });

     }
}
var table2;
if(getlogsbtn != null ){
    if(getlogsbtn.length !=0){
        var getbtndata = JSON.parse(getlogsbtn);
        for (var i = 0; i < getbtndata.length; i++) {  
              getbtndata[i].logs = false;
              alllogsbtn.push(getbtndata[i]);
               
          }
             // console.log("updated seen "+getdata);  
    localStorage.setItem('btnlogs', JSON.stringify(alllogsbtn));
    //console.log(localStorage.getItem('logs'));            
      table2 = $('#example2').DataTable ({
          //"data" : getbtndata.reverse(),
          "data" : getbtndata,
          "ordering": false,
          "columns" : [
              { "data" : "Button",'title' : 'Button' },
              { "data" : "message",'title' : 'Message'  },
              { "data" : "time",'title' : 'Time'  },
              {
                data: null,
                className: "center",
                defaultContent: '<a href="" class="editor_remove">Clear</a>'
              }
          ]
      });
      
      // Delete a record
     $('#example2').on('click', 'a.editor_remove', function (e) {
        e.preventDefault();
        console.log($(this).closest('tr'));
        var rowIndex = $(this).closest('tr')[0]['sectionRowIndex'];
        $(this).closest('tr').remove();
        let getlogsbtn = localStorage.getItem('btnlogs');
        var getbtndata = JSON.parse(getlogsbtn);
        var temLogs = [...getbtndata];
        temLogs.sort(function(a, b){return b.valid_date - a.valid_date});
        console.log(temLogs);
        temLogs.splice(rowIndex, 1); 
        var temLogsNew = [...temLogs];
        temLogsNew.sort(function(a, b){return a.valid_date - b.valid_date});
        console.log(temLogsNew);
        localStorage.setItem('btnlogs', JSON.stringify(temLogs));
        //table2.ajax.reload();
     });
   }
}

function clearlogs(){
    //return new Promise((resolve) => {
        var getlogs = localStorage.getItem('logs');
        var alllogs = [];
        if(getlogs != null){
            if(getlogs.length !=0){
                var  getdata = JSON.parse(getlogs);
                for (var i = 0; i < getdata.length; i++) {
                    if(getdata[i].logs == true || (new Date() > new Date(getdata[i].valid_date))){
                       alllogs.push(getdata[i]);
                    }
                }
            }
        }
        localStorage.setItem('logs', JSON.stringify(alllogs));
        var getlogs = localStorage.getItem('btnlogs');
        var alllogs = [];
        if(getlogs != null){
            if(getlogs.length !=0){
                var  getdata = JSON.parse(getlogs);
                for (var i = 0; i < getdata.length; i++) {
                    if(getdata[i].logs == true || (new Date() > new Date(getdata[i].valid_date))){
                       alllogs.push(getdata[i]);
                    }
                }
            }
        }
        localStorage.setItem('btnlogs', JSON.stringify(alllogs));
        //resolve('cleared');
    //})
}
$('#clear_logs').on('click',function(){
    
    clearlogs();
    //clear datatable
    if(table1)
        table1.clear().draw();
    if(table2)
    table2.clear().draw();
});
